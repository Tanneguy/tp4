function showhidedescription(){
	$(this).find(".description").toggle();
}

function hidemenu(){
		$(".type").remove();	
		$("button").remove();
		$(".row p").remove();
}


$(document).ready(function(){

	//variables
	var total = 0;
	var pizza = 0;
	var pizzaPrice = 0;
	var patePrice = 0;
	var extraPrice = 0;
	var nom = 0;
	var slices = 1;
	

	$(".pizza-type label").hover(showhidedescription, showhidedescription);

	$(".nb-parts input").on('keyup', function(){
        $(".pizza-pict").remove();

        pizza = $('<span class="pizza-pict"></span>');

        slices = +$(this).val();

        for(i=0;i< slices/6; i++){
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);

    })


	$(".next-step").click(function(){
		//remove button
		$(this).remove();

		//affiche info client
		$(".infos-client").show();
	});

	$(".add").click(function(){
		$ (this).before('<br> <input type="text">'); 	
	});


	$(".done").click(function(){
		// cache le menu et les choix
		nom = $(".infos-client .type:first input").val();

		//var txtDone = text("Merci " + nom + " !" + " Votre commande sera livrée dans 15 minutes");  // Create text with jQuery
		$(".main").empty().append("Merci " + nom + " !" + " Votre commande sera livrée dans 15 minutes");   // Append new elements
		
		hidemenu();
	});

	//Calucule du prix total
	function price(fpizzaPrice, fpatePrice, fextraPrice){
		total = fpizzaPrice + fpatePrice +fextraPrice;

		if (total == 0) {
			$(".tile p").text("0 €");
		} else if(total!==0){                
			$(".tile p").text(total.toPrecision(3) +"€");
		}
	}

	//récupère le prix de la pizza choisi
    $(".pizza-type input").click(function() {
			
		pizzaPrice = $(this).data('price');

		pizzaPrice = (pizzaPrice / 6) * slices ;

		price(pizzaPrice,patePrice,extraPrice);
	});
	
	
	//récupère le prix de la pâte choisi
	$(".type input[name='pate']").click(function(){

		patePrice = $(this).data('price');
		price(pizzaPrice,patePrice, extraPrice);

	});

	//récupère le prix des extras choisis
	$(".type input[name='extra']").click(function(){
		extraPrice = 0;
		$('input[name=extra]:checked').each(function() { 
			extraPrice += $(this).data('price'); 
		});
		price(pizzaPrice, patePrice, extraPrice);
	});

});